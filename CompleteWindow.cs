using System;
using UnityEngine;
using UnityEngine.UI;

namespace Appside.Main
{
    public class CompleteWindow : AWindow
    {
        public event Action OnContinue = delegate {  };
        [SerializeField] private Button _continueButton;

        private void Awake()
        {
            _continueButton.onClick.AddListener(() => { OnContinue();});
        }
    }
}