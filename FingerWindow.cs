using System;
using UnityEngine;
using UnityEngine.UI;

namespace Appside.Main
{
    public class FingerWindow : AWindow
    {
        public event Action OnClicked = delegate {  };
        [SerializeField] private Button _button; 
        
        private void Awake()
        {
            _button.onClick.AddListener(() => { OnClicked();});
        }
    }
}